<?php
/**
 * Created by PhpStorm.
 * User: zippyttech
 * Date: 23/07/18
 * Time: 04:06 PM
 */

namespace App\Http\Repositories;


class TatucoRepository
{
    protected $model;

    public function __construct($model)
    {
        $this->model = $model;
    }

    public function index()
    {
       // return User::all();
        $query = $this->model::select("*");

        return $query;
    }

    public function show($id)
    {
        $item = $this->model::find($id);

        return $item;
    }

    public function store($data)
    {
        $new = $this->model::create($data);

        return $new;
    }

    public function update($data)
    {
        $item = $this->model::update($data);

        return $item;
    }
}