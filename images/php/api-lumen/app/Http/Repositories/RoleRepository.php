<?php
/**
 * Created by PhpStorm.
 * User: zippyttech
 * Date: 23/07/18
 * Time: 05:26 PM
 */

namespace App\Http\Repositories;


use App\Acl\Src\Models\Role;

class RoleRepository extends TatucoRepository
{
    public function __construct()
    {
        parent::__construct(new Role());
    }
}