<?php
/**
 * Created by PhpStorm.
 * User: zippyttech
 * Date: 23/07/18
 * Time: 04:32 PM
 */

namespace App\Http\Controllers;


use App\Http\Services\UserService;

class UserController extends TatucoController
{
    public function __construct()
    {
        parent::__construct(new UserService());
    }
}