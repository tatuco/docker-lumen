<?php
/**
 * Created by PhpStorm.
 * User: zippyttech
 * Date: 23/07/18
 * Time: 04:05 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class TatucoController extends BaseController
{

    public $service;

    public function __construct($service){
        $this->service = $service;
    }


    public function index()
    {
        return $this->service->_index();
    }

    public function show($id)
    {
        return $this->service->_show($id);
    }

    public function _store(Request $request)
    {
        return $this->service->store($request);
    }


    public function _update($id, $request)
    {

        return $this->service->_update($id, $request);
    }

    public function destroy($id, Request $request)
    {
        return $this->service->_destroy($id, $request);
    }


}