<?php

namespace App\Http\Controllers;

use App\Http\Services\PermissionService;

class PermissionController extends TatucoController
{

    public function __construct()
    {
        parent::__construct(new PermissionService());
    }

}
