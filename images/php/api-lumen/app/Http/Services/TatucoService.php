<?php
/**
 * Created by PhpStorm.
 * User: zippyttech
 * Date: 23/07/18
 * Time: 04:06 PM
 */

namespace App\Http\Services;


use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class TatucoService
{

    protected $model;
    protected $object;
    protected $name = "item";
    protected $namePlural = "items";
    protected $paginate = false;
    protected $limit = null;
    protected $data = [];
    protected $request;
    protected $dato;
    protected $repository;

    public function __construct($repository)
    {
        $this->repository = $repository;
    }

    public function _index(){
        try{

                $query = $this->repository->index();
                Log::info("{$query->toSql()}");
                if(!$query)
                {
                    return response()->json([
                        "message"=> "no hay registros"
                    ], 200);
                }

                return response()->json([
                    "list" => $query->get(),
                    "count"=> count($query->get())
                ], 200);

        }catch (\Exception $e){
            return $this->errorException($e);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * consultar un registro por medio de un id
     */
    public function _show($id, $request = null)
    {
        try{

            $this->data = $this->repository->show($id);

            if(!$this->data)
            {
                return response()->json(['message'=>$this->name. ' no existe'], 404);
            }


            Log::info('Encontrado');

            return response()->json([
                $this->name => $this->data,
            ], 200);

        }catch (\Exception $e){
            return $this->errorException($e);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonRespon
     * guardar un registro nuevo
     */
    public function _store(Request $request)
    {
        try{

            if (count($this->data) == 0) {
                $this->data = $request->all();
            }

            Log::info('Guardado');
            if($this->object = $this->repository->store($this->data)){
                return response()->json([
                    $this->name => $this->object],
                    201);
            }

        }catch (\Exception $e){
            return $this->errorException($e);
        }
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * acualizar registro
     */
    public function _update($id, Request $request)
    {
        try {

            $this->object = $this->model->find($id);

            if (!$this->object) {
                return response()->json([
                    'message' => $this->name . ' no existe'
                ], 404);
            }
            $this->data = $request->all();
            if (!$this->repository->update($this->data)){
                return response()->json([
                    'status'=>false,
                    'message'=>'No se pudo Modificar',
                    $this->name=> $this->object
                ], 200);
            }

            return response()->json([
                'status'=>true,
                'message'=>$this->name. ' Modificado',
                $this->name=> $this->object
            ], 200)->setStatusCode(200, "Registro Actualizado");

        }catch(\Exception $e){
            return $this->errorException($e);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * metodo para eliminar un registro
     */
    public function _destroy($id, $request)
    {
        try {

            $this->object = $this->model::find($id);

            if (!$this->object) {
                return response()->json([
                    'message' => $this->name . ' no existe'
                ], 404);
            }

            $this->object->update();
            return response()->json([
                'status' => true,
                'message' => $this->name . ' Eliminado'
            ], 206);
        }catch (\Exception $e){
            return $this->errorException($e);
        }
    }

    /**
     * @param $id
     * @return bool
     * metodo para verificar si un registro existe o no
     * con el fin de devolver una respuesta de no encontrado
     */
    public function findByItem($id)
    {
        if(!$this->model->find($id))
            return false;
        else
            return true;
    }

    /**
     * @param null $item
     * @return \Illuminate\Http\JsonResponse
     * respuesta de no encontrado en formato json
     * puede recibir un string para escificar que cosa no se encontro
     */
    public function notFound($item = null)
    {
        return response()->json([
            'message'=> $item.' No Encontrado'
        ],404);
    }


    public function errorException(\Exception $e)
    {
        Log::critical("Error, archivo del peo: {$e->getFile()}, linea del peo: {$e->getLine()}, el peo: {$e->getMessage()}");
        return response()->json([
            "message" => "Error de servidor",
            "exception" => $e->getMessage(),
            "file" => $e->getFile(),
            "line" => $e->getLine(),
            "code" => $e->getCode(),
           // "error" => $this->runError()
        ], 500);
    }


    public function runError()
    {
        $dir = __DIR__;
        $salida = shell_exec('mpg123 '.__DIR__.'/sonidos/error500.mp3');
        echo $salida;
    }

    public function getNow()
    {
        return Carbon::now()->format('Y-m-d H:i:s');
    }

}