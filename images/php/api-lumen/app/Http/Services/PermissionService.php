<?php

namespace App\Http\Services;

use App\Http\Repositories\PermissionRepository;

class PermissionService extends TatucoService
{

    public function __construct()
    {
        parent::__construct(new PermissionRepository());
    }
}