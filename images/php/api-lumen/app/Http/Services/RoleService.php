<?php

namespace App\Http\Services;

use App\Http\Repositories\RoleRepository;

class RoleService extends TatucoService
{

    public function __construct()
    {
        parent::__construct(new RoleRepository());
    }

}