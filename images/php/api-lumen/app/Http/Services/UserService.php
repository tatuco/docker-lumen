<?php
/**
 * Created by PhpStorm.
 * User: zippyttech
 * Date: 23/07/18
 * Time: 04:33 PM
 */

namespace App\Http\Services;


use App\Http\Repositories\UserRepository;

class UserService extends TatucoService
{
    public function __construct()
    {
        parent::__construct(new UserRepository());
    }

}