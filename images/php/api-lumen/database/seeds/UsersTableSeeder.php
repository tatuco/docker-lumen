<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\User::class)->create([
            'name' => 'AdminZippyttech',
            'email' => 'sysadmin@zippyttech.com',
            'password' => password_hash('123456', PASSWORD_BCRYPT)
        ]);
    }
}
