<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


$router->group(['prefix' => 'api'], function () use ($router) {

    $router->post('users/role', 'UserController@assignedRole');
    $router->get('users/role/{user}/{role}', 'UserController@revokeRole');
    $router->post('roles/permission', 'RoleController@assignedPermission');
    $router->get('roles/permission/{role}/{permission}', 'RoleController@revokePermission');

    $router->get('/users', "UserController@index");
    $router->get('/users/{id}', "UserController@show");


    $router->get('/roles', "RoleController@index");

    $router->get('/permissions', "PermissionController@index");

    $router->group(['middleware' => 'auth'], function () use ($router) {
        $router->get('/test', function () use ($router) {
            return "ruta con oauth2";
        });
    });
});

